def add (a,b)
  a + b
end

def subtract (a, b)
  a - b
end

def sum (arr)
  arr.reduce(0,:+)
end

def multiply (*numbers)
  numbers.reduce(1,:*)
end

def pow (base, power)
  base**power
end

def factorial (num)
  return 1 if num == 0
  num * factorial(num - 1)
end
