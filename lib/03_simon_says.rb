def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, frq = 2)
  ((str + ' ') * frq)[0..-2]
end

def start_of_word(word, idx)
  word[0...idx]
end

def first_word(sentence)
  sentence.split(' ').first
end

EXCEPTIONS = ["the", "and", "over",
              "or", "to", "a", "but",
              "by", "for", "in", "of"]

def titleize(phrase)
  phrase.split.map.with_index do |wrd, i|
    if EXCEPTIONS.include?(wrd) && i != 0
       wrd
    else
       wrd.capitalize
    end
  end.join(' ')
end
